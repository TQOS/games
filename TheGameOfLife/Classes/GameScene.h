#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "Grid.h"

class GameScene: public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // implement the "static create()" method manually
	CREATE_FUNC(GameScene);

private:
	Grid* _grid;
	cocos2d::ui::Text* _populationCount;
	cocos2d::ui::Text* _generationCount;

	cocos2d::ui::Button* playButton;
	cocos2d::ui::Button* pauseButton;

	void play(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
	void pause(Ref* pSender, cocos2d::ui::Widget::TouchEventType type);
	
	void gameOver();
	void replay();

	void step(float dt);
};

#endif // __GAME_SCENE_H__
