#include "Creature.h"

using namespace cocos2d;

bool Creature::init() {
	if (!Sprite::initWithFile("Assets/SpriteImages/bubble.png")) {
		return false;
	}

	this->setLivingNeighborCount(0);
	this->setIsAlive(false);

	return true;
}

void Creature::setLivingNeighborCount(int livingNeightborsCount) {
	_livingNeighborsCount = livingNeightborsCount;
}

int Creature::livingNeighborCount() {
	return _livingNeighborsCount;
}

void Creature::setIsAlive(bool isAlive) {
	this->setVisible(isAlive);
	_isAlive = isAlive;
}

bool Creature::alive() {
	return _isAlive;
}