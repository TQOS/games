#ifndef __CREATURE_H__
#define __CREATURE_H__

#include "cocos2d.h"

class Creature : public cocos2d::Sprite {
public:
	CREATE_FUNC(Creature);

	bool init() override;

	void setLivingNeighborCount(int livingNeightborsCount);
	int livingNeighborCount();

	void setIsAlive(bool isAlive);
	bool alive();

protected:
	int  _livingNeighborsCount;
	bool _isAlive;
};

#endif // __CREATURE_H__