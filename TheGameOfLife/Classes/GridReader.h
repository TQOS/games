#ifndef __GRID_READER_H__
#define __GRID_READER_H__

#include "cocos2d.h"
#include "cocostudio\WidgetReader\NodeReader\NodeReader.h"

class GridReader: public cocostudio::NodeReader {
public:
	static GridReader* getInstance();
	static void purge();

	cocos2d::Node* createNodeWithFlatBuffers(const flatbuffers::Table* nodeOptions);
};

#endif // __GRID_READER_H__