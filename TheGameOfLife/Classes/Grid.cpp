#include "Grid.h"

using namespace cocos2d;

const int ROWS = 8;
const int COLUMNS = 10;

bool Grid::init() {
	if (!Node::init()) {
		return false;
	}

	_generationCount = 0;
	_populationCount = 0;

	return true;
}

void Grid::onEnter() {
	Node::onEnter();

	this->setupGrid();

	this->setupTouchHandling();
}

void Grid::setupGrid() {
	Sprite* gridSprite = this->getChildByName<Sprite*>("grid");

	_cellWidth  = gridSprite->getContentSize().width  / float(COLUMNS);
	_cellHeight = gridSprite->getContentSize().height / float(ROWS);

	_gridArr.reserve(ROWS * COLUMNS);

	for (int row = 0; row < ROWS; ++row) {
		for (int col = 0; col < COLUMNS; ++col) {
			Creature* creature = Creature::create();
			if (!creature) {
				continue;
			}

			creature->setAnchorPoint(Vec2(0.0f, 0.0f));
			creature->setPosition(_cellWidth * float(col), _cellHeight * float(row));
			
			gridSprite->addChild(creature);

			_gridArr.pushBack(creature);
		}
	}
}

void Grid::setupTouchHandling() {
	auto mouseListener = EventListenerMouse::create();

	mouseListener->onMouseDown = [&](Event* event) {
		EventMouse* e = (EventMouse*)event;

		Sprite* gridSprite = (Sprite*)this->getChildByName("grid");

		Vec2 gridMouseLocation = gridSprite->convertToNodeSpace(e->getLocationInView());

		Creature* touchedCreature = this->creatureForTouchLocation(gridMouseLocation);

		if (touchedCreature) {
			touchedCreature->setIsAlive(!touchedCreature->alive());
		}
	};
	/*auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = [&](Touch* touch, Event* event) {
		Sprite* gridSprite = (Sprite*)this->getChildByName("grid");

		Vec2 gridTouchLocation = gridSprite->convertTouchToNodeSpace(touch);

		Creature* touchedCreature = this->creatureForTouchLocation(gridTouchLocation);

		if (touchedCreature) {
			touchedCreature->setIsAlive(!touchedCreature->alive());
		}
		return true;
	};*/

	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(mouseListener, this);
}

#pragma mark -
#pragma mark Public Functions

void Grid::evolveStep() {
	this->updateNeightborCount();	// update each Creature's neighbor count
	this->updateCreatures();		// update each Creature's state

	++_generationCount;				// update the generation so the label's text will display the correct generation
}

int Grid::generationCount() {
	return _generationCount;
}

int Grid::populationCount() {
	return _populationCount;
}

#pragma mark -
#pragma mark Private Functions

void Grid::updateNeightborCount() {
	for (int row = 0; row < ROWS; ++row) {
		for (int col = 0; col < COLUMNS; ++col) {			
			int currentCreatureIndex  = this->indexForRowColumn(row, col);

			Creature* currentCreature = _gridArr.at(currentCreatureIndex);
			currentCreature->setLivingNeighborCount(0);
			
			bool outVerticalBorder = false;										// State out from border left or right side

			for (int nRow = row - 1; nRow <= row + 1; ++nRow) {					// Checking all neighbors are they alive. We are checking eight cell
				int tnRow = nRow;												// For continue loop, if nRow equal -1.
				if (nRow == -1) {												// if out border from left side then 
					nRow = ROWS - 1;											// shift index on last row
				}
				
				bool outHorizontalBorder = false;								// State out from border top or bottom side 

				for (int nCol = col - 1; nCol <= col + 1; ++nCol) {				
					int tnCol = nCol;
					
					if (nCol == -1) {
						nCol = COLUMNS - 1;
					}

					bool indexValid = this->isValidIndex(nRow, nCol);

					if (indexValid && !(nRow == row && nCol == col)) {
						int neighborIndex = this->indexForRowColumn(nRow, nCol);
						Creature* neighbor = _gridArr.at(neighborIndex);

						if (neighbor->alive()) {
							int livingNeighbors = currentCreature->livingNeighborCount();
							currentCreature->setLivingNeighborCount(livingNeighbors + 1);
						}
					}

					if (outHorizontalBorder) {
						break;
					}

					nCol = tnCol;
					if (!outHorizontalBorder && nCol + 1 >= COLUMNS && col == COLUMNS - 1) {
						nCol = -1;
						outHorizontalBorder = true;
					}
				}

				if (outVerticalBorder) {
					break;
				}
				nRow = tnRow;
				if (!outVerticalBorder && nRow + 1 >= ROWS && row == ROWS - 1) {
					nRow = -1;
					outVerticalBorder = true;
				}
			}
			
		}
	}
}

void Grid::updateCreatures() {
	_populationCount = 0;
	
	for (int row = 0; row < ROWS; ++row) {
		for (int col = 0; col < COLUMNS; ++col) {
			int currentCreatureIndex = this->indexForRowColumn(row, col);
			Creature* currentCreature = _gridArr.at(currentCreatureIndex);
			
			int livingNeigbors = currentCreature->livingNeighborCount();

			if (livingNeigbors == 3) {
				currentCreature->setIsAlive(true);
			}
			else if (livingNeigbors <= 1 || livingNeigbors >= 4) {
				currentCreature->setIsAlive(false);
			}

			if (currentCreature->alive()) {
				++_populationCount;
			}
		}
	}
}

Creature* Grid::creatureForTouchLocation(cocos2d::Vec2 touchLocation) {
	if (touchLocation.x < 0.0f || touchLocation.y < 0.0f) {
		return nullptr;
	}

	int row = touchLocation.y / _cellHeight;
	int col = touchLocation.x / _cellWidth;

	if (this->isValidIndex(row, col)) {
		return _gridArr.at(this->indexForRowColumn(row, col));
	}

	return nullptr;
}

#pragma mark - 
#pragma mark Grid Array Utility Functions

bool Grid::isValidIndex(int row, int col) {
	return (row >= 0 && row < ROWS) && (col >= 0 && col < COLUMNS);
}

int Grid::indexForRowColumn(int row, int col) {
	return row * COLUMNS + col;
}