#include "GameScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "GridReader.h"

USING_NS_CC;

using namespace cocostudio::timeline;

Scene* GameScene::createScene() {
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = GameScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameScene::init() {
    // 1. super init first
    if ( !Layer::init() ) {
        return false;
    }
    
	CSLoader* instance = CSLoader::getInstance();
	instance->registReaderObject("GridReader", (ObjectFactory::Instance)GridReader::getInstance);

    auto rootNode = CSLoader::createNode("MainScene.csb");
	
	Size size = Director::getInstance()->getVisibleSize();
	rootNode->setContentSize(size);
	ui::Helper::doLayout(rootNode);

	auto leftPanel  = rootNode->getChildByName("leftPanel");
	auto rightPanel = rootNode->getChildByName("rightPanel");
	
	_grid = rightPanel->getChildByName<Grid*>("gridNode");
	if (!_grid) {
		log("Error -> grid don't loaded.\n\r");
		return false;
	}

	auto balloon = leftPanel->getChildByName("balloon");
	_generationCount = balloon->getChildByName<cocos2d::ui::Text*>("generationCount");
	_populationCount = balloon->getChildByName<cocos2d::ui::Text*>("populationCount");

	playButton = leftPanel->getChildByName<cocos2d::ui::Button*>("btnPlay");
	pauseButton = leftPanel->getChildByName<cocos2d::ui::Button*>("btnPause");

	playButton->addTouchEventListener(CC_CALLBACK_2(GameScene::play, this));
	pauseButton->addTouchEventListener(CC_CALLBACK_2(GameScene::pause, this));

    addChild(rootNode);

    return true;
}

void GameScene::play(Ref* pSender, cocos2d::ui::Widget::TouchEventType type) {
	this->schedule(CC_SCHEDULE_SELECTOR(GameScene::step), 0.5f);
}

void GameScene::pause(Ref* pSender, cocos2d::ui::Widget::TouchEventType type) {
	this->unschedule(CC_SCHEDULE_SELECTOR(GameScene::step));
}

void GameScene::step(float dt) {
	_grid->evolveStep();

	auto populationCount = _grid->populationCount();

	_generationCount->setString(std::to_string(_grid->generationCount()));
	_populationCount->setString(std::to_string(populationCount));

	if (populationCount == 0) {
		pause(pauseButton, cocos2d::ui::Widget::TouchEventType::ENDED);
		_grid->pauseSchedulerAndActions();
		gameOver();
	}
}

void GameScene::replay() {
	Director::getInstance()->replaceScene(GameScene::createScene());
}

void GameScene::gameOver() {
	String* text = String::create("The end!");

	auto visibleSize = Director::getInstance()->getVisibleSize();
	auto scenePos = getPosition();

	LabelTTF* label = LabelTTF::create(text->getCString(), "Fonts\\Courier New Bold.ttf", 40);
	label->setPosition(visibleSize.width / 2 - (label->getContentSize().width / 2) + 150, visibleSize.height / 2 + label->getContentSize().height / 2);

	auto slideIn = MoveBy::create(1.0f, Vec2(0.0f, visibleSize.height / 2 + label->getContentSize().height / 2 + 50.0f));

	MenuItemImage* menuItemReplay = MenuItemImage::create("menu\\replay.png", "menu\\replay.png", "menu\\replay.png", CC_CALLBACK_0(GameScene::replay, this));
	auto menu = Menu::createWithItem(menuItemReplay);
	menu->setPosition(scenePos.x + (visibleSize.width / 2) - (label->getContentSize().width / 2) + 150, -100.0f);

	this->addChild(menu, 30);
	this->addChild(label, 31);

	menu->runAction(slideIn);
}