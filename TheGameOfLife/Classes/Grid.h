#ifndef __GRID_H__
#define __GRID_H__

#include "cocos2d.h"
#include "Creature.h"

class Grid : public cocos2d::Node {
public:
	CREATE_FUNC(Grid);

	bool init() override;

	void onEnter() override;

	void evolveStep();

	int generationCount();

	int populationCount();

protected:
	int _generationCount;
	int _populationCount;
	
	float _cellWidth;
	float _cellHeight;

	cocos2d::Vector<Creature*> _gridArr;

	void setupGrid();
	void setupTouchHandling();
	void updateNeightborCount();
	void updateCreatures();

	Creature* creatureForTouchLocation(cocos2d::Vec2 touchLocation);
	bool isValidIndex(int row, int col);
	int indexForRowColumn(int row, int col);
	cocos2d::Vec2 tileCoordForPosition(cocos2d::Vec2 position);
};

#endif // __GRID_H__