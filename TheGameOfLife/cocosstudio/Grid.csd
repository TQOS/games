<GameFile>
  <PropertyGroup Name="Grid" Type="Node" ID="978fd7c1-1cb0-46e5-bb04-ed624a5c969a" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" CustomClassName="Grid" Tag="8" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="grid" ActionTag="39027928" Tag="9" IconVisible="False" LeftMargin="-380.0000" RightMargin="-380.0000" TopMargin="-307.0000" BottomMargin="-307.0000" ctype="SpriteObjectData">
            <Size X="760.0000" Y="614.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="SpriteImages/grid.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>