#include "GameLevelScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

using namespace cocostudio::timeline;


Scene* GameLevelScene::createScene() {
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = GameLevelScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameLevelScene::init() {
    if (!Layer::init()) {
        return false;
    }
    
	_gameOver = false;

    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	_tileMap = TMXTiledMap::create("levels\\1\\level1.tmx");					  // init map for level 1
	this->addChild(_tileMap, 5);

	_walls	 = _tileMap->layerNamed("walls");
	_hazards = _tileMap->layerNamed("hazards");

	LayerColor* layerBlueSky = LayerColor::create(Color4B(100, 100, 250, 255), _tileMap->getContentSize().width, visibleSize.height); // init background of level
	this->addChild(layerBlueSky, 4);

	_player = Player::create( "sd\\koalio_stand.png" );
	_player->setPosition( Vec2(100, 50) );
	this->addChild(_player, 15);

	auto keyListener = EventListenerKeyboard::create();
	keyListener->onKeyPressed  = CC_CALLBACK_2(GameLevelScene::onKeyPressed, this);
	keyListener->onKeyReleased = CC_CALLBACK_2(GameLevelScene::onKeyReleased, this);
	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyListener, this);

	this->scheduleUpdate();

	auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
	audio->preloadBackgroundMusic("levels\\1\\level1.mp3");
	audio->preloadEffect("general_sounds\\hurt.wav");
	audio->preloadEffect("general_sounds\\jump.wav");

	audio->playBackgroundMusic("levels\\1\\level1.mp3");

    return true;
}

void GameLevelScene::update(float dt) {
	if (_gameOver) {
		return;
	}

	_player->update(dt);

	hazardCollision(_player);
	checkForWin();
	checkForAndResolveCollisions(_player);
	
	setViewPointCenter(_player->getPosition());
}

Vec2 GameLevelScene::tileCoordForPosition( const Vec2& position ) {
	float x = floor(position.x / _tileMap->getTileSize().width);
	float levelHeightInPixels = _tileMap->getMapSize().height * _tileMap->getTileSize().height;
	float y = floor((levelHeightInPixels - position.y) / _tileMap->getTileSize().height);
		
	return Vec2(x, y);
}

Rect GameLevelScene::tileRectFromCoord(const Vec2& tileCoords) {
	float levelHeightInPixels = _tileMap->getMapSize().height * _tileMap->getTileSize().height;
	Vec2  origin = ccp(tileCoords.x * _tileMap->getTileSize().width, levelHeightInPixels - ((tileCoords.y + 1) * _tileMap->getTileSize().height));
	return Rect(origin.x, origin.y, _tileMap->getTileSize().width, _tileMap->getTileSize().height);
}

Array* GameLevelScene::getSurroundingTilesAtPosition( const Vec2& position, TMXLayer* layer ) {
	Vec2 plPos = this->tileCoordForPosition(position);

	Array* gids = Array::create();

	for (int i = 0; i < 9; ++i) {
		int c = i % 3;
		int r = (int)(i / 3);
		Vec2 tilePos = Vec2(plPos.x + (c - 1), plPos.y + (r - 1));
		
		if (tilePos.y > (_tileMap->getMapSize().height - 1)) { // fallen in a hole
			this->gameOver(false);
			return NULL;
		}

		int tgid = layer->tileGIDAt(tilePos);

		Rect tileRect = this->tileRectFromCoord(tilePos);

		Dictionary* tileDict = Dictionary::create();
		tileDict->setObject(Integer::create(tgid), "gid");
		tileDict->setObject(Float::create(tileRect.origin.x), "x");
		tileDict->setObject(Float::create(tileRect.origin.y), "y");
		tileDict->setObject(Float::create(tilePos.x), "tilePosX");
		tileDict->setObject(Float::create(tilePos.y), "tilePosY");

		log("gid = %i, [%f,%f]", tgid, tilePos.x, tilePos.y);

		gids->addObject(tileDict);
	}
	//CCARRAY_FOREACH(gids, obj) {
	//	Dictionary* dict = (Dictionary*)obj;
	//	log("x: %f; y: %f", dict->valueForKey("x")->getCString(), dict->valueForKey("y")->getCString());
	//}
	gids->removeObjectAtIndex(4);
	gids->insertObject(gids->objectAtIndex(2), 6);
	gids->removeObjectAtIndex(2);
	gids->exchangeObjectAtIndex(4, 6);
	gids->exchangeObjectAtIndex(0, 4);
	
	//log("special block {");
	//Ref* obj;
	//CCARRAY_FOREACH(gids, obj) {
	//	Dictionary* dict = (Dictionary*)obj;
	//	int tgid = ((Integer*)dict->objectForKey("gid"))->getValue();
	//	float x = ((Float*)dict->objectForKey("x"))->getValue();
	//	float y = ((Float*)dict->objectForKey("y"))->getValue();
	//	log("b-> sgid = %i, [%f,%f]", tgid, x, y);
	//}
	//log("}");

	return gids;
}

Rect rectIntersections(const Rect& r1, const Rect& r2) {
	Rect intersection = Rect(MAX(r1.getMinX(), r2.getMinX()), MAX(r1.getMinY(), r2.getMinY()), 0, 0);

	intersection.size.width  = MIN(r1.getMaxX(), r2.getMaxX()) - intersection.getMinX();
	intersection.size.height = MIN(r1.getMaxY(), r2.getMaxY()) - intersection.getMinY();

	return intersection;
}

void GameLevelScene::checkForAndResolveCollisions(Player* player) {
	Array* tiles = getSurroundingTilesAtPosition(player->getPosition(), _walls);
	if (_gameOver) {
		return;
	}
	player->setOnGround(false);

	Ref* obj;
	CCARRAY_FOREACH(tiles, obj) {
		Dictionary* dict = (Dictionary*)obj;
		
		int gid = ((Integer*)dict->objectForKey("gid"))->getValue();
		
		if (gid) {
			float x = ((Float*)dict->objectForKey("x"))->getValue();
			float y = ((Float*)dict->objectForKey("y"))->getValue();

			Rect playerRect = player->collisionBoundingBox();

			Rect tileRect(x, y, _tileMap->getTileSize().width, _tileMap->getTileSize().height);

			if (playerRect.intersectsRect(tileRect)) {
				Rect intersection = rectIntersections(playerRect, tileRect);
				
				int tileIndex = tiles->getIndexOfObject(dict);

				Vec2 position = player->getDesiredPosition();

				switch (tileIndex) {
					case 0:	// tile is directly below Koala
						player->setDesiredPosition(Vec2(position.x, position.y + intersection.size.height));
						player->setVelocity(Vec2(player->velocity().x, 0.0f));
						player->setOnGround(true);
						break;
					case 1:	// tile is directly above Koala
						player->setDesiredPosition(Vec2(position.x, position.y - intersection.size.height));
						player->setVelocity(Vec2(player->velocity().x, 0.0f));
						break;
					case 2:	// tile is left of Koala
						player->setDesiredPosition(Vec2(position.x + intersection.size.width, position.y));
						break;
					case 3:	// tile is right of Koala
						player->setDesiredPosition(Vec2(position.x - intersection.size.width, position.y));
						break;
					default:
						if (intersection.size.width > intersection.size.height) { // resolve diagonal vertically collision
							player->setVelocity(Vec2(player->velocity().x, 0.0f));
							float intersectionHeight;
							if (tileIndex > 5) {
								intersectionHeight = intersection.size.height;
								player->setOnGround(true);
							} else {
								intersectionHeight = -intersection.size.height;
							}
							player->setDesiredPosition(Vec2(player->getDesiredPosition().x, player->getDesiredPosition().y + intersectionHeight/*intersection.size.height*/));
						}
						else {													 // resolve diagonal horizontally collision
							float resolutionWidth;
							if (tileIndex == 6 || tileIndex == 4) {
								resolutionWidth = intersection.size.width;
							} else {
								resolutionWidth = -intersection.size.width;
							}
							player->setDesiredPosition(Vec2(player->getDesiredPosition().x, player->getDesiredPosition().y + resolutionWidth));
						}
				} // switch (tileIndex)
			} // if (playerRect.intersectsRect(tileRect)) {...}
		} // gid
	} // CCARRAY_FOREACH

	player->setPosition(player->getDesiredPosition());
}

void GameLevelScene::hazardCollision(Player* player) {
	Array* tiles = getSurroundingTilesAtPosition(player->getPosition(), _hazards);

	CCObject* obj;
	CCARRAY_FOREACH(tiles, obj) {
		Dictionary* dict = (Dictionary*)obj;
		
		int gid = ((Integer*)dict->objectForKey("gid"))->getValue();
		
		if (!gid) {
			continue;
		}

		int x = ((Float*)dict->objectForKey("x"))->getValue();
		int y = ((Float*)dict->objectForKey("y"))->getValue();

		Rect playerBB = player->getBoundingBox();
		Rect tileRect(x, y, _tileMap->getTileSize().width, _tileMap->getTileSize().height / 4); 

		if (playerBB.intersectsRect(tileRect)) {
			gameOver(false);
			return;
		}
	}

}

void GameLevelScene::gameOver(bool won) {
	_gameOver = true;

	String* text;
	if (won) {
		text = String::create("YOU WON!");
	} else {
		text = String::create("You have DIED!");
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("general_sounds\\hurt.wav");
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	auto scenePos = getPosition();
	scenePos.x = abs(scenePos.x);

	LabelTTF* label = LabelTTF::create(text->getCString(), "Marker Felt", 40);
	label->setPosition(scenePos.x + (visibleSize.width / 2) - (label->getContentSize().width / 2) + 150, visibleSize.height / 2 + label->getContentSize().height / 2);
	
	auto slideIn = MoveBy::create(1.0f, Vec2(0.0f, visibleSize.height / 2 + label->getContentSize().height / 2 + 50.0f));
		
	MenuItemImage* menuItemReplay = MenuItemImage::create("menu\\replay.png", "menu\\replay.png", "menu\\replay.png", CC_CALLBACK_0(GameLevelScene::replay, this));
	auto menu = Menu::createWithItem(menuItemReplay);
	menu->setPosition(scenePos.x + (visibleSize.width / 2) - (label->getContentSize().width / 2) + 150, -100.0f);
	
	this->addChild(menu , 30);
	this->addChild(label, 31);

	menu->runAction(slideIn);
}

void GameLevelScene::replay() {
	_gameOver = false;
	Director::getInstance()->replaceScene(GameLevelScene::createScene());
}

void GameLevelScene::checkForWin() {
	if (_player->getPosition().x > 3130.0f) {
		gameOver(true);
	}
}


void GameLevelScene::onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, Event* event) {
	switch (keyCode) {
		case EventKeyboard::KeyCode::KEY_UP_ARROW:
			_player->setJump(true);
			break;
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			_player->setForward(true);
			break;
	}
}

void GameLevelScene::onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, Event* event) {
	switch (keyCode) {
		case EventKeyboard::KeyCode::KEY_UP_ARROW:
			_player->setJump(false);
			break;
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
			_player->setForward(false);
			break;
	}
}

void GameLevelScene::setViewPointCenter(Vec2 position) {
	Size winSize = Director::getInstance()->getWinSize();

	int x = MAX(position.x, winSize.width / 2);
	int y = MAX(position.y, winSize.height / 2);
	x = MIN(x, (_tileMap->getMapSize().width * _tileMap->getTileSize().width) - winSize.width / 2);
	y = MIN(y, (_tileMap->getMapSize().height * _tileMap->getTileSize().height) - winSize.height / 2);

	Vec2 actualPosition(x, 0);

	Vec2 centerOfView = Vec2(winSize.width / 2, 0);
	Vec2 viewPoint = centerOfView - actualPosition;
	
	this->setPosition(viewPoint);
}