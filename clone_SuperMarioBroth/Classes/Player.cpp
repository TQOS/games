#include "Player.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Player::Player(): Sprite() {
	_velocity = Vec2( 0.0f, 0.0f );
	_forwardMarch = false;
	_mightAsWellJump = false;
}
Player* Player::create(const std::string& filename) {
	Player* player = new Player();
	if ( player && player->initWithFile( filename ) ) {
		return player;
	}

	CC_SAFE_DELETE(player);
	return NULL;
}

void Player::update(float dt) {
	Vec2 gravity(0.0f, -100.0f);
	Vec2 gravityStep = gravity * dt;

	Vec2 forwardMove(800.0f, 0.0f);
	Vec2 forwardStep = forwardMove * dt;

	_velocity += gravityStep;
	_velocity = Vec2(_velocity.x * 0.90, _velocity.y);

	Vec2 jumpForce(0.0f, 310.0f);
	float jumpCutoff = 150.0f;

	if (_mightAsWellJump && _onGround) { // jumping
		_velocity += jumpForce;
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("general_sounds\\jump.wav");
	}
	else if (!_mightAsWellJump && _velocity.y > jumpCutoff) {
		_velocity = Vec2(_velocity.x, jumpCutoff);
	}

	if (_forwardMarch) {
		_velocity += forwardStep;
	}

	Vec2 minMovement(0.0f, -450.0f);
	Vec2 maxMovement(120.0f, 250.0f);
	_velocity = ccpClamp(_velocity, minMovement, maxMovement);

	Vec2 stepVelocity = _velocity * dt;

	_desiredPosition = getPosition() + stepVelocity;
}

Rect Player::collisionBoundingBox() {
	Rect collisionBox = getBoundingBox();
	// CGRectInset
	collisionBox.size.width -= 6;
	collisionBox.origin.x	+= 3;

	Vec2 diff = _desiredPosition - getPosition();
	// CGRectOffset
	collisionBox.origin += diff;

	return collisionBox;
}

Vec2 Player::getDesiredPosition() {
	return _desiredPosition;
}

void Player::setDesiredPosition(const Vec2& position) {
	_desiredPosition = position;
}

bool Player::isOnGround() {
	return _onGround;
}

void Player::setOnGround(const bool& state) {
	_onGround = state;
}

cocos2d::Vec2 Player::velocity() const {
	return _velocity;
}

void Player::setVelocity(const cocos2d::Vec2& velocity) {
	_velocity = velocity;
}

bool Player::isForward() {
	return _forwardMarch;
}
void Player::setForward(const bool& state) {
	_forwardMarch = state;
}

void Player::setJump(const bool& state) {
	_mightAsWellJump = state;
}