#ifndef __GAMELEVEL_SCENE_H__
#define __GAMELEVEL_SCENE_H__

#include "cocos2d.h"
#include "Player.h"

class GameLevelScene : public cocos2d::Layer {
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();

    // implement the "static create()" method manually
    CREATE_FUNC( GameLevelScene );

	void update( float delta );

	cocos2d::Vec2 tileCoordForPosition(const cocos2d::Vec2& position);
	cocos2d::Rect tileRectFromCoord(const cocos2d::Vec2& tileCoord);
	cocos2d::Array* getSurroundingTilesAtPosition(const cocos2d::Vec2& position, cocos2d::TMXLayer* layer);
	
	void checkForAndResolveCollisions(Player* player);
	void hazardCollision(Player* player);

	void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
	void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);

	void setViewPointCenter(cocos2d::Vec2 position);

	void gameOver(bool won);
	void checkForWin();

	void replay();
	
private:
	cocos2d::TMXTiledMap* _tileMap;
	cocos2d::TMXLayer*	  _walls;
	cocos2d::TMXLayer*	  _background;
	cocos2d::TMXLayer*	  _hazards;

	Player*				  _player;

	bool				 _gameOver;
};

#endif // __GAMELEVEL_SCENE_H__
