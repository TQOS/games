#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "cocos2d.h"

class Player: public cocos2d::Sprite {
public:
	Player();
	
	static Player* create(const std::string& filename);

	void update( float delta );
	
	cocos2d::Rect collisionBoundingBox();

	cocos2d::Vec2 getDesiredPosition();
	void setDesiredPosition(const cocos2d::Vec2& position);

	bool isOnGround();
	void setOnGround(const bool& state);

	bool isForward();
	void setForward(const bool& state);

	void setJump(const bool& state);

	cocos2d::Vec2 velocity() const;
	void setVelocity(const cocos2d::Vec2& velocity);

private:
	cocos2d::Vec2  _force;
	cocos2d::Vec2  _velocity;
	cocos2d::Vec2  _acceleration;
	cocos2d::Vec2  _desiredPosition;

	bool _onGround;
	bool _forwardMarch;
	bool _mightAsWellJump;
};

#endif // __PLAYER_H__
